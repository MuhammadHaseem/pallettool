<?php
/**
 * Plugin Name:       Plate Tool
 * Plugin URI:        logicsbuffer.com/
 * Description:       Wall Plate designer Tool [platetool]
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Mustafa jamal
 * Author URI:        logicsbuffer.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       plate-tool
 * Domain Path:       /languages
 */

	add_action( 'init', 'pallettool' );

	function pallettool() {

		//wp_dequeue_script('jquery');
        //wp_deregister_script('jquery');
        //wp_dequeue_script('jquery-core-js');

        //wp_enqueue_script( 'lbpt_jquery', plugins_url().'/plate-tool/js/jquery.min.js');
        //add_shortcode( 'show_custom_fancy_product', 'custom_fancy_product_form' );
		add_shortcode( 'platetool', 'pallet_tool_single' );
		add_action( 'wp_enqueue_scripts', 'chop_tool_script' );
		//add_action( 'wp_ajax_nopriv_post_love_calculateQuote', 'post_love_calculateQuote' );
		//add_action( 'wp_ajax_post_love_calculateQuote', 'post_love_calculateQuote' );
		// Setup Ajax action hook
		// add_action( 'wp_ajax_read_me_later', array( time(), 'read_me_later' ) );
		// add_action( 'wp_ajax_nopriv_read_me_later', array( time(), 'read_me_later' ) );
		// add_action( 'wp_ajax_read_me_later',  'read_me_later' );
		// add_action( 'wp_ajax_nopriv_read_me_later', 'read_me_later' );

		//wp_localize_script( 'my_voter_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        
		//wp_enqueue_script( 'my_voter_script' );

		//Default Crop
		//update_option( 'attachment_url', "N/A");

	}




	function chop_tool_script() {
			        
		//wp_enqueue_script( 'rt_custom_fancy_font_observer', plugins_url().'/plate-tool/js/fontfaceobserver.js',array(),time());	
		
				
		//wp_enqueue_script( 'lbpt_jquery_color', plugins_url().'/plate-tool/js/jquery.color.min.js');		
		//wp_enqueue_script( 'rt_fabric_script', plugins_url().'/plate-tool/js/fabric.js');
		//wp_enqueue_script( 'lbpt_jquery_ui', plugins_url().'/plate-tool/js/jquery-ui.js');
		wp_enqueue_script( 'pd_jcrop', plugins_url().'/plate-tool/js/Jcrop.js');
		wp_enqueue_script( 'lbpt_main_script', plugins_url().'/plate-tool/js/mainscript.js',array(),time());	
		//wp_enqueue_script( 'lbpt_fabric_script', plugins_url().'/plate-tool/js/fabric2-4.min.js');
		wp_enqueue_script( 'lbpt_jassor_slider', plugins_url().'/plate-tool/js/jssor.slider-28.1.0.min.js');
		//wp_enqueue_script( 'pd_jcrop', plugins_url().'/plate-tool/js/jcrop.js',array(),time());
		//wp_enqueue_script( 'rt_jquery_ui', plugins_url().'/plate-tool/js/jquery-ui.js');
		//wp_enqueue_script( 'pd_cropper', plugins_url().'/plate-tool/js/cropper.js');
		
		//Jcrop Jquery
		
		//wp_enqueue_script( 'pd_jquery', plugins_url().'/plate-tool/js/jquery.min.js');

		//wp_enqueue_script( 'pd_cropper_jquery', plugins_url().'/plate-tool/js/jquery-cropper.min.js');
		
		wp_enqueue_style( 'lbpt_main_style', plugins_url().'/plate-tool/css/main.css',array(),time());
		//wp_enqueue_style( 'lbpt_cropper_css', plugins_url().'/plate-tool/css/cropper.min.css',array(),time());

		//Jcrop CSS
		wp_enqueue_style( 'lbpt_jquery_jcrop_css', plugins_url().'/plate-tool/css/jquery.Jcrop.css',array(),time());
		//wp_enqueue_style( 'lbpt_jcrop_main_css', plugins_url().'/plate-tool/css/jcrop_main.css',array(),time());
		//wp_enqueue_style( 'lbpt_bootstrap', plugins_url().'/plate-tool/css/bootstrap.min.css',array(),time());
		//wp_enqueue_style( 'rt_custom_fancy_style_responsive', plugins_url().'/plate-tool/css/custom_fancy_responsive.css',array(),time());
		//wp_enqueue_style( 'rt_custom_fancy_jqeury_ui', plugins_url().'/plate-tool/css/jquery-ui.min.css',array(),time());
	}

function wpa_process_form(){
	//Crop Form Starts
	if(isset($_POST['lb_crop_submit'])){
	    $clicked_image = $_POST['lb_clicked_image'];
		$x = $_POST['lb_x'];
		$y = $_POST['lb_y'];
		$w = $_POST['lb_cropperw'];
		//$h = $_POST['lb_cropperh'];
		$h = $_POST['lb_h'];
	    $jpeg_quality = 90;

	    $src = $clicked_image;
	    $img_r = imagecreatefromjpeg($src);
	    //print_r($w);
	    //echo "**********";
	    //print_r($h);
	    $dst_r = ImageCreateTrueColor( $w, $h );

	    imagecopyresampled($dst_r,$img_r,0,0,$x,$y,$w,$h,$w,$h);
	    //header('Content-type: image/jpeg');	
	    ob_start();
	   
	    //imagejpeg($dst_r,'https://duschrueckwand-online.de/wp-content/plugins/plate-tool/simpletext.jpg',$jpeg_quality);
	    
	    imagejpeg($dst_r,null,$jpeg_quality);
	    
	    $cropped_image = ob_get_clean();
	    //ob_end_clean();

	    // Free up memory
		//$cropped_image = imagedestroy($dst_r);

	    $base64 = base64_encode( $cropped_image );
	    //$attachment = chunk_split($base64);
	    //echo $attachment;
	    //echo "<img src='data:image/jpeg;base64,".$base64."'>";
	    
	    $title = "croppediamge";
	    $attachemntid = save_image( $base64, $title );
	    //echo $attachemntid;
	    $attachment_url = wp_get_attachment_url( $attachemntid );
	    //echo $attachment_url;
	    update_option( 'attachment_url', $attachment_url );
	    $attachment_url_option = get_option( 'attachment_url');
	    echo "<input id='cropped_url' type='hidden' value='".$attachment_url_option."'>";
	    //update_option( 'cropped_image_url', $attachment_url );
	    ?>
	
	    <?php
	}
	   // else{
	// 	update_option( 'attachment_url', "N/A");
	// }
	//Crop Form Ends
}
add_action( 'init', 'wpa_process_form' );

function pallet_tool_single($atts) {
	
	//Get current Product id for cart details
	global $post;
	$product_id = $post->ID;
	update_option('current_p_tool_id',$product_id);

	extract(shortcode_atts(array(
	  'home' => 0,
	), $atts));
	//Check whether it is home page
	if ($home == 1) {
		$product_id = 19172;
	}else{		
		global $product;
		global $post;
		//$post->ID;
		$product_id = $post->ID;
	}

	$plugins_url =  plugins_url( 'images/icons/', __FILE__ );
	global $woocommerce;
	global $product;
	//$product_id = $post->ID;

	if(isset($_POST['submit_prod'])){
			
		// //$total_qty = $_POST['rt_qty'];			 
		$quantity = 1;			 
		
		$rt_total_price = $_REQUEST['total_price'];
		$nmber_of_pallets = $_REQUEST['nmber_of_pallets'];
		echo $rt_total_price;
		//$woocommerce->cart->add_to_cart( $product_id, $quantity ); 
		update_post_meta($product_id, 'total_price', $rt_total_price);
		update_post_meta($product_id, 'nmber_of_pallets', $nmber_of_pallets);
		// $rt_total_price = $_POST['pricetotal_quote'];
		// echo $rt_total_price;
		// echo $product_id;
		   
		//Set price
		//$product_id = $post->ID;    
	    //$myPrice = get_post_meta('total_price_quote');

	    // Get the WC_Product Object instance
		//$product = wc_get_product( $product_id );

		// Set the product active price (regular)
		//$product->set_price( $rt_total_price );
		//$product->set_regular_price( $rt_total_price ); // To be sure

		// Save product data (sync data and refresh caches)
		//$product->save();
		//Set price end

		// echo "**** P ***";
		// echo $product_id;
		// echo "**** P ***";

		//die();
		// Cart item data to send & save in order
		$cart_item_data = array(
			'custom_price' => $rt_total_price		
		);   
		//print_r($cart_item_data);
		//die();
		//$variation_id = null;
		//$variation = null;

		// woocommerce function to add product into cart check its documentation also 
		// what we need here is only $product_id & $cart_item_data other can be default.
		//WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation, $cart_item_data);
		
		global $woocommerce;
		//$woocommerce->cart->add_to_cart( $product_id, $quantity,$cart_item_data );

		WC()->cart->add_to_cart(  $product_id, $quantity, $variation_id, $variation, $cart_item_data);
		 //Calculate totals
		WC()->cart->calculate_totals();
		 //Save cart to session
		WC()->cart->set_session();
		 //Maybe set cart cookies
		WC()->cart->maybe_set_cart_cookies();	    

	}

	
	
	//$page_title = get_the_title();
	//$terms = get_the_terms( get_the_ID(), 'product_cat' );
	//$product_type = $terms[0]->slug;
	ob_start();
	?>
	<form role="form" action="" id="add_cart" enctype="multipart/form-data" method="post">						
	<?php 
		if ($home[0] == 1) {
			$product_id = 19172;
		}else{
			global $product;
			global $post;
			$product_id = $post->ID;
		}
		$range_1_from = get_field('range_1_from', $product_id);
		$range_1_to = get_field('range_1_to', $product_id);
		$range_1_price = get_field('range_1_price', $product_id);
		$range_2_from = get_field('range_2_from', $product_id);
		$range_2_to = get_field('range_2_to', $product_id);
		$range_2_price = get_field('range_2_price', $product_id);
		$price_above_range_2 = get_field('price_above_range_2', $product_id);

		$alu_premium_matt = get_field('alu_premium_matt', $product_id);	
		$alu_premium_glanz = get_field('alu_premium_glanz', $product_id);	
		$alu_struktur_optik = get_field('alu_struktur_optik', $product_id);	
		$hart_pvc = get_field('hart_pvc', $product_id);	
	?>	
	<div id="choptoolmain" class="custom_calculator single_product">

		<input type="hidden" name="nmber_of_pallets" id="nmber_of_pallets" value="">

		<input type="hidden" id="range_1_from" value="<?php echo $range_1_from; ?>">
		<input type="hidden" id="range_1_to" value="<?php echo $range_1_to; ?>">
		<input type="hidden" id="range_1_price" value="<?php echo $range_1_price; ?>">
		<input type="hidden" id="range_2_from" value="<?php echo $range_2_from; ?>">
		<input type="hidden" id="range_2_to" value="<?php echo $range_2_to; ?>">
		<input type="hidden" id="range_2_price" value="<?php echo $range_2_price; ?>">
		<input type="hidden" id="price_above_range_2" value="<?php echo $price_above_range_2; ?>">
		<input type="hidden" id="selected_mat_price" name="selected_mat_price" value="">
		
		<input type="hidden" id="alu_premium_matt" name="alu_premium_matt" value="<?php echo $alu_premium_matt; ?>">
		<input type="hidden" id="alu_premium_glanz" name="alu_premium_glanz" value="<?php echo $alu_premium_glanz; ?>">
		<input type="hidden" id="alu_struktur_optik" name="alu_struktur_optik" value="<?php echo $alu_struktur_optik; ?>">
		<input type="hidden" id="hart_pvc" name="hart_pvc" value="<?php echo $hart_pvc; ?>">

	    <div class="container tabs-slider-main">
		        	<div class="maintabsmenu col col-12 col-sm-12 col-md-12 col-lg-12">			        
					    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1280px;height:235px;overflow:hidden;visibility:hidden;">

				        <div data-u="loading" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-position:50% 50%;background-repeat:no-repeat;background-image:url(<?php plugins_url().'/plate-tool/img/double-tail-spin-x.svg' ?>);background-color:rgba(0, 0, 0, 0.7);background-size:40px 40px;"></div>
				        <div data-u="slides" id="tabsselection" class="gallerytabs" style="cursor:default;position:relative;top:0px;left:0px;width:1280px;height:150px;overflow:hidden;">
				            <div data-val="1" class="tabssel tab1"><span>BERGSEE<span></span></div>
				        	<div data-val="2" class="tabssel tab2"><span>STRAND & MEER</span></div>        	
				        	<div data-val="3" class="tabssel tab3"><span>KINDERMOTIV</span></div>        	
				        	<div data-val="4" class="tabssel tab3"><span>Wasserfall</span></div>        	
				        	<div data-val="5" class="tabssel tab3"><span>Maritim</span></div>        	
				        	<div data-val="6" class="tabssel tab3"><span>Wellness</span></div>        	
				        	<div data-val="7" class="tabssel tab3"><span>Beton-Stein-Marmor</span></div>        	
				        	<div data-val="8" class="tabssel tab3"><span>Fliesenoptik</span></div>        	
				        	<div data-val="9" class="tabssel tab3"><span>Küchenrückwand</span></div>        	
				        	<div data-val="10" class="tabssel tab3"><span>Holzoptik</span></div>        	
				        	<div data-val="11" class="tabssel tab3"><span>Trendwand</span></div>        	
				        	<div data-val="12" class="tabssel tab3"><span>Schwarz Weiß</span></div>        	
				        	<div data-val="13" class="tabssel tab3"><span>Blumen & Blätter</span></div>        	
				        	<div data-val="14" class="tabssel tab3"><span>Unifarben</span></div>        	
				        	<div data-val="15" class="tabssel tab3"><span>Neuheiten</span></div>        	
				        	<div data-val="16" class="tabssel tab3"><span>Metalloptik</span></div> 
				        </div><a data-scale="0" href="https://www.jssor.com" style="display:none;position:absolute;">animation</a>
				        <div data-u="navigator" class="jssorb057" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
				            <div data-u="prototype" class="i" style="width:14px;height:14px;">
				                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
				                    <circle class="b" cx="8000" cy="8000" r="5000"></circle>
				                </svg>
				            </div>
				        </div>
				        <div data-u="arrowleft" class="jssora073" style="width:50px;height:50px;top:0px;left:-2px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
				            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
				                <path class="a" d="M4037.7,8357.3l5891.8,5891.8c100.6,100.6,219.7,150.9,357.3,150.9s256.7-50.3,357.3-150.9 l1318.1-1318.1c100.6-100.6,150.9-219.7,150.9-357.3c0-137.6-50.3-256.7-150.9-357.3L7745.9,8000l4216.4-4216.4 c100.6-100.6,150.9-219.7,150.9-357.3c0-137.6-50.3-256.7-150.9-357.3l-1318.1-1318.1c-100.6-100.6-219.7-150.9-357.3-150.9 s-256.7,50.3-357.3,150.9L4037.7,7642.7c-100.6,100.6-150.9,219.7-150.9,357.3C3886.8,8137.6,3937.1,8256.7,4037.7,8357.3 L4037.7,8357.3z"></path>
				            </svg>
				        </div>
				        <div data-u="arrowright" class="jssora073 rightarrow" style="width:50px;height:50px;top:0px;right:15px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
				            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
				                <path class="a" d="M11962.3,8357.3l-5891.8,5891.8c-100.6,100.6-219.7,150.9-357.3,150.9s-256.7-50.3-357.3-150.9 L4037.7,12931c-100.6-100.6-150.9-219.7-150.9-357.3c0-137.6,50.3-256.7,150.9-357.3L8254.1,8000L4037.7,3783.6 c-100.6-100.6-150.9-219.7-150.9-357.3c0-137.6,50.3-256.7,150.9-357.3l1318.1-1318.1c100.6-100.6,219.7-150.9,357.3-150.9 s256.7,50.3,357.3,150.9l5891.8,5891.8c100.6,100.6,150.9,219.7,150.9,357.3C12113.2,8137.6,12062.9,8256.7,11962.3,8357.3 L11962.3,8357.3z"></path>
				            </svg>
				        </div>
				        <div id="imagegalleriesparent" class="">	
						        <div class="categoryicons">
									<div data-val="1" class="galcontent sea tab1content">
										<?php echo do_shortcode('[ngg src="galleries" ids="1" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="2" class="galcontent waterfall tab2content">
										<?php echo do_shortcode('[ngg src="galleries" ids="2" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="3" class="galcontent waterfall tab3content">
										<?php echo do_shortcode('[ngg src="galleries" ids="3" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="4" class="galcontent waterfall tab4content">
										<?php echo do_shortcode('[ngg src="galleries" ids="4" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="5" class="galcontent waterfall tab5content">
										<?php echo do_shortcode('[ngg src="galleries" ids="5" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="6" class="galcontent waterfall tab6content">
										<?php echo do_shortcode('[ngg src="galleries" ids="6" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="7" class="galcontent waterfall tab7content">
										<?php echo do_shortcode('[ngg src="galleries" ids="7" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="8" class="galcontent waterfall tab8content">
										<?php echo do_shortcode('[ngg src="galleries" ids="8" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="9" class="galcontent waterfall tab9content">
										<?php echo do_shortcode('[ngg src="galleries" ids="9" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="10" class="galcontent waterfall tab10content">
										<?php echo do_shortcode('[ngg src="galleries" ids="10" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="11" class="galcontent waterfall tab11content">
										<?php echo do_shortcode('[ngg src="galleries" ids="11" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="12" class="galcontent waterfall tab12content">
										<?php echo do_shortcode('[ngg src="galleries" ids="12" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="13" class="galcontent waterfall tab13content">
										<?php echo do_shortcode('[ngg src="galleries" ids="13" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="14" class="galcontent waterfall tab14content">
										<?php echo do_shortcode('[ngg src="galleries" ids="14" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="15" class="galcontent waterfall tab15content">
										<?php echo do_shortcode('[ngg src="galleries" ids="15" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
									<div data-val="16" class="galcontent waterfall tab16content">
										<?php echo do_shortcode('[ngg src="galleries" ids="16" display="basic_thumbnail" override_thumbnail_settings="1" thumbnail_width="180" thumbnail_height="120" images_per_page="6" ajax_pagination="1" thumbnail_crop="0" number_of_columns="6"]');?>
									</div>
								</div>	
						</div>	
				    </div>
				</div>
			</div>
	   </div>
	  	<div class="toolleftparent fusion-fullwidth fullwidth-box fusion-builder-row-9 ">
		  	<div class="row">
				<div class="toolsidebars toolleft col medium-2 col-3 col-sm-12 col-md-3 col-lg-2">
					<div id="detailSelect">
						<div class="material_head"> 
							<h3>Material<span style="" class="helpmaterial">?</span></h3>
						</div>
						<div class="matbox" id="mat1" data-title="Alu Premium Matt" data-price="<?php echo $alu_premium_matt; ?>" style="cursor: pointer;">
							<font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Alu Premium Matt</font></font>
						</div>
						<div class="matbox" id="mat2" data-title="Alu Premium Glanz" data-price="<?php echo $alu_premium_glanz; ?>" style="cursor: pointer;">
							<font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Alu Premium Glanz</font></font>
						</div>
						<div class="matbox matactive" data-title="Alu Struktur Optik" id="mat3" data-price="<?php echo $alu_struktur_optik; ?>" style="cursor: pointer;">
							<font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Alu Struktur Optik</font></font>
						</div>
						<div class="matbox" id="mat4" data-title="Hart PVC" data-price="<?php echo $hart_pvc; ?>" style="cursor: pointer;">
							<font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Hart PVC</font></font>
						</div>
					</div>
					<div class="plateselect">
							<h3>Platten</h3>
							<div class="platecount" id="platec1" data-val="1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1</font></font></div>
						<div class="platecount" id="platec2" data-val="2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2</font></font></div>
						<div class="platecount plateactive" id="platec3" data-val="3"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3</font></font></div>
					</div>
					<div>
						<h3>SERVICE</h3>
						<p>Grafikservice: 30 €</p>
					</div>
					<div>
						<h3 style="font-size: 18px;">Muster bestellen <span style="cursor: pointer;" class="helpMuster pum-trigger">?</span></h3>
					</div>
			    </div>
			    <div class="col col-8 col-sm-12 col-md-8 col-lg-8 text-center mainarea" style="white-space: nowrap;" id="configCenter">
	
			  	  <button type="button" id="cropper_model" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Ausschnitt wählen Spiegeln</button>
					<div id="myModal" class="modal">

					    <div class="modal-content">
					  		<span class="close">&times;</span>					  	
					    	<div class="croppermain">
								<h2 id="modalTitle">Ausschnitt wählen</h2>							
						        <div class="img-container">
						        	<img src="" id="cropbox1" />
						        </div>
						            <div style="display: none;">
						                <input type="hidden" name="lb_clicked_image" id="lb_clicked_image" />
						                <label>X1 <input type="text" name="lb_x" id="lb_x" size="4"/></label><br/>
						                <label>Y1 <input type="text" name="lb_y" id="lb_y" size="4"/></label><br/>
						                <label>X2 <input type="text" name="lb_x2" id="lb_x2" size="4"/></label><br/>
						                <label>Y2 <input type="text" name="lb_y2" id="lb_y2" size="4"/></label><br/>
						                <label>W <input type="text" name="lb_w" id="lb_w" size="4"/></label><br/>
						                <label>W cropped <input type="text" name="lb_cropperw" id="lb_cropperw" /></label><br/>
						                <label>H cropped <input type="text" name="lb_cropperh" id="lb_cropperh" /></label><br/>
						                <label>H <input type="text" name="lb_h" id="lb_h" size="4"/></label>
						            </div>
						            <div style="margin:5px;">
						                <input type="button" name="lb_crop_submit" class="lb_crop_submit" id="lb_crop_submit" value="Crop Image" />
						            </div>
							</div>
					    </div>

					</div>
			      
			      <div class="heightcontainer" style="font-size: 12px; margin-bottom: 10px;">
			         Höhe: <input type="number" style="max-width: 62px; font-size: 14px; padding: 2px; margin: 2px; text-align: center;" name="pHeight" id="pHeight" value="2000" size="4" maxlength="4"> mm<br>
			         <div class="cmstyle" style="margin-top: -5px;" id="pHeightcm">150 cm</div>
			      </div>
			      <canvas id="canvas" ></canvas>
			      <div id="palletarea" style="width:800px;height: 700px;">
			      	<div id="pallet0"></div>
			      	<div id="pallet1"></div>
			      	<div id="pallet2"></div>
			      </div>

			      <div align="center" id="mainplateswrap">
			         <span class="verticalImage2" id="vcol0" style="max-width: 93%; text-align: right;">
			         	<img src="" id="pimg0" style="max-height: 580px; height: 579.875px;">
			         </span>
			         <span class="verticalImage2" id="vcol1" style="max-width: 6%; text-align: left;">
			         	<img src="" id="pimg1" style="max-height: 580px; height: 579.875px;">
			         </span>
			         <span class="verticalImage3" id="vcol2" style="display: none;">
			         	<img src="maker/blank.png" id="pimg2">
			         </span>
			      </div>
			      <div align="center" id="imageName" class="cmstyle">Shop_BS_09_20.jpg</div>
			      <div align="center">
			         <span style="font-size: 12px; width: 50%;" class="verticalImage2" id="wInput0">
			            <span>Breite:</span><br>
			            <input type="number" style="max-width: 62px; font-size: 14px; padding: 2px; margin: 2px; text-align: center;" value="900" name="width0" id="width0" size="4" maxlength="4"> mm
			            <div class="cmstyle" style="margin-top: -5px;" id="width0cm">150 cm</div>
			         </span>
			         <span style="font-size: 12px; width: 50%;" class="verticalImage2" id="wInput1">
			            <span>Breite:</span><br>
			            <input type="number" style="max-width: 62px; font-size: 14px; padding: 2px; margin: 2px; text-align: center;" value="900" name="width1" id="width1" size="4"> mm
			            <div class="cmstyle" style="margin-top: -5px;" id="width1cm">10 cm</div>
			         </span>
			         <span style="font-size: 12px; display: none;" class="verticalImage3" id="wInput2">
			            <span>Breite:</span><br>
			            <input type="number" style="max-width: 62px; font-size: 14px; padding: 2px; margin: 2px; text-align: center;" value="900" name="width2" id="width2" size="4"> mm
			            <div class="cmstyle" style="margin-top: -5px;" id="width2cm">90 cm</div>
			         </span>
			      
					
			     </div>
			    </div>
			    <?php
					//$product_id = $post->ID;
					
					//Check whether it is home page
					//$home = get_post_meta($product_id, 'home_check', false);
					if ($home[0] == 1) {
						$product_id = 19172;
					}else{
						global $product;
						global $post;
						$product_id = $post->ID;
					}
					// echo "***";
					// print_r($home);
					// echo "*******";
					// echo $product_id;
					// echo "*******";
					$product = wc_get_product( $product_id );
					//print_r($product);
					if( $product instanceof WC_Product ){
					  $product_price_woo = $product->get_price();
					}

					// $product = wc_get_product( $product_id );
					// $product_price_woo = $product->get_regular_price();
				?>
			    <div class="toolsidebars toolright col medium-2 col-3 col-sm-12 col-md-3 col-lg-2">
			    	<div class="righttext">
			    		<h4 class="rightheading">GESAMTPREIS:</h4>
			    		<p><span id="total_price_second">€ <?php echo $product_price_woo;?></span> €inkl. Mwst.</p>
			    	</div>
			    	<div class="righttext">
			    		<h4 class="rightheading">VERSANDKOSTENFREI</h4>
			    		<p>innerhalb Deutschlands (Inselzuschlag 79,- €)</p>
			    	</div>
			    	<div class="righttext">
			    		<h4 class="rightheading">PRODUKTIONSZEIT</h4>
			    		<p>Bearbeitungszeit 10 Tage</p>
			    	</div>
			    	<div class="icon-box-text last-reset">									
						<div id="text-857485485" class="text total_price_right">
							<input type="hidden" name="range_price" id="range_price" value="">		
							<input type="hidden" name="total_price_default" id="total_price_default" value="<?php echo $product_price_woo;?>">		
							<input type="hidden" name="total_price" id="total_price" value="<?php echo $product_price_woo;?>">
							<input type="hidden" id="no-of-pallets" name="no-of-pallets" >
							<input type="hidden" id="selected_material" name="selected_material">
							<input type="hidden" id="selected_image" name="selected_image">
							<input type="hidden" id="cropped_image" name="cropped_image">
							<h3><span id="total_price">€ <?php echo $product_price_woo;?></span> inkl. Mwst</h3>		
						</div>
					</div>
			    	<div class="add-cart-btn-main">					
						<input type="submit" name="submit_prod" class="add_to_cart_btn tool-buttons-text" value="In den Warenkorb" />
						<span class="tool-buttons-icon" style="display:none;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"><metadata><x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.6-c138 79.159824, 2016/09/14-01:09:01"><rdf:rdf xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"><rdf:description rdf:about=""></rdf:description></rdf:rdf></x:xmpmeta></metadata>
						<defs><filter id="filter" x="884" y="987" width="24" height="24" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"></feFlood><feComposite result="composite" operator="in" in2="SourceGraphic"></feComposite><feBlend result="blend" in2="SourceGraphic"></feBlend></filter></defs><path id="Forma_1_copy" data-name="Forma 1 copy" class="cls-1" d="M907.79,992.453a0.934,0.934,0,0,0-.728-0.346H890.081l-0.069-.64,0-.021A5.129,5.129,0,0,0,884.937,987a0.938,0.938,0,0,0,0,1.875,3.249,3.249,0,0,1,3.212,2.809l1.114,10.239a2.809,2.809,0,0,0-1.654,2.56v0.05a2.814,2.814,0,0,0,2.813,2.81H890.8a2.766,2.766,0,1,0,5.237,0h4.045a2.742,2.742,0,0,0-.148.89,2.766,2.766,0,1,0,2.766-2.76H890.422a0.939,0.939,0,0,1-.938-0.94v-0.05a0.939,0.939,0,0,1,.938-0.94H902.39a4.3,4.3,0,0,0,3.891-2.62,0.937,0.937,0,1,0-1.717-.75,2.446,2.446,0,0,1-2.174,1.5H891.122l-0.837-7.689h15.624l-0.459,2.2a0.939,0.939,0,0,0,.727,1.11,0.969,0.969,0,0,0,.192.019,0.938,0.938,0,0,0,.917-0.746l0.694-3.328A0.936,0.936,0,0,0,907.79,992.453ZM902.7,1007.34a0.89,0.89,0,1,1-.891.89A0.892,0.892,0,0,1,902.7,1007.34Zm-9.281,0a0.89,0.89,0,1,1-.891.89A0.891,0.891,0,0,1,893.422,1007.34Z" transform="translate(-884 -987)"></path>
						</svg></span>
					</div>
			    </div>
		    </div>
	    </div>
	    <div class="container palfooter">
		  	<div class="row">
				<div class="col medium-4 col-4 col-sm-2 col-md-4 col-lg-4"></div>			
				<div class="col medium-4 col-4 col-sm-12 col-md-4 col-lg-4 resetbutton">
					<!-- <button type="button" id="save_size" class="btn btn-primary">Save Size</button> -->
					<button type="button" id="reset" class="btn btn-primary">Reset</button>					
				</div>			
				<div class="col medium-4 col-4 col-sm-2 col-md-4 col-lg-4"></div>			
			</div>			
		</div>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.1.3/cropper.js"></script>		
	</div>

	</form>			
	<?php 
	
	$toolcontent = ob_get_contents(); //Gives whatever has been "saved"
	ob_end_clean(); //Stops saving things and discards whatever was saved
	ob_flush(); //Stops saving and outputs it all at once
	return $toolcontent;
}
	function platetool(){
		if (get_field('custom_tool') == 'Enable') {
			echo do_shortcode("[platetool]");
		}
	}
	add_action( 'woocommerce_after_single_product_summary', 'platetool', 10 );
	//Add data to cart
	//add_action('woocommerce_add_order_item_meta','order_meta_handler', 1, 3);
	//add_filter('woocommerce_add_cart_item_data', 'add_cart_item_custom_data', 10, 2);

	// function order_meta_handler($item_id, $values, $cart_item_key) {
	// 	// Allow plugins to add order item meta

	// 	$cart_session = WC()->session->get('cart');
	// 	// if($cart_session[$cart_item_key]['rt_total_price'][0]){
	// 	// wc_add_order_item_meta($item_id, "Total Price", $cart_session[$cart_item_key]['rt_total_price'][0]);
	// 	// }
	// 	if($cart_session[$cart_item_key]['nmber_of_pallets'][0]){
	// 		wc_add_order_item_meta($item_id, "Number of Pallets", $cart_session[$cart_item_key]['nmber_of_pallets'][0]);
	// 	}
		
	// }
	// function add_cart_item_custom_data($cart_item_meta, $new_post_id) {
	// 	global $woocommerce;
	// 	//Check whether it is home page
	// 	//$home = get_post_meta($product_id, 'home_check');
	// 	if ($home[0] == 1) {
	// 		$product_id = 19172;
	// 	}else{
	// 		global $product;
	// 		global $post;
	// 		$product_id = $post->ID;
	// 	}
	// 	//$rt_total_price = get_post_meta($product_id, 'rt_total_price');
	// 	$nmber_of_pallets = get_post_meta($product_id, 'nmber_of_pallets');
	// 	echo "***********Cart item *************";
	// 	echo $nmber_of_pallets;
	// 	echo "***********Cart item *************";
	// 	die();
	// 	return $cart_item_meta;
	// }

/**
 * Add engraving text to cart item.
 *
 * @param array $cart_item_data
 * @param int   $product_id
 * @param int   $variation_id
 *
 * @return array
 */
function iconic_add_engraving_text_to_cart_item( $cart_item_data, $product_id, $variation_id ) {
	

	//if($cart_item_data['pHeight'] == ""){
		
	//}else{
		$no_of_pallets = filter_input( INPUT_POST, 'no-of-pallets' );
		$selected_material = filter_input( INPUT_POST, 'selected_material' );
		$width0 = filter_input( INPUT_POST, 'width0' );
		$width1 = filter_input( INPUT_POST, 'width1' );
		$width2 = filter_input( INPUT_POST, 'width2' );
		$width2 = filter_input( INPUT_POST, 'width2' );
		$pHeight = filter_input( INPUT_POST, 'pHeight' );
		$selected_image = filter_input( INPUT_POST, 'selected_image' );
		//$cropped_image = filter_input( INPUT_POST, 'cropped_image' );
		$cropped_image = get_option( 'attachment_url');

		$cart_item_data['no-of-pallets'] = $no_of_pallets;
		$cart_item_data['selected_material'] = $selected_material;
		$cart_item_data['width0'] = $width0;
		$cart_item_data['width1'] = $width1;
		$cart_item_data['width2'] = $width2;
		$cart_item_data['pHeight'] = $pHeight;
		$cart_item_data['selected_image'] = $selected_image;
		$cart_item_data['cropped_image'] = $cropped_image;
		return $cart_item_data;
	//}
}

/**
 * Display engraving text in the cart.
 *
 * @param array $item_data
 * @param array $cart_item
 *
 * @return array
 */
function iconic_display_engraving_text_cart( $item_data, $cart_item ) {
	if(is_null($cart_item["no-of-pallets"])){
		return $item_data;
	}else{
		$anzahl = "";
	$item_data[] = array(
		'key'     => __( '<br/>*Nur bei Auswahl über Konfigurator', 'iconic' ),
		'value'   => wc_clean( $anzahl ),
		'display' => '',
	);
	$item_data[] = array(
		'key'     => __( 'Anzahl Platten', 'iconic' ),
		'value'   => wc_clean( $cart_item['no-of-pallets'] ),
		'display' => '',
	);
	$item_data[] = array(
		'key'     => __( 'Material', 'iconic' ),
		'value'   => wc_clean( $cart_item['selected_material'] ),
		'display' => '',
	);
	$item_data[] = array(
		'key'     => __( 'Platte 1 Breite', 'iconic' ),
		'value'   => wc_clean( $cart_item['width0'] ),
		'display' => '',
	);
	$item_data[] = array(
		'key'     => __( 'Platte 2 Breite', 'iconic' ),
		'value'   => wc_clean( $cart_item['width1'] ),
		'display' => '',
	);
	$item_data[] = array(
		'key'     => __( 'Platte 3 Breite', 'iconic' ),
		'value'   => wc_clean( $cart_item['width2'] ),
		'display' => '',
	);
	$item_data[] = array(
		'key'     => __( 'Höhe', 'iconic' ),
		'value'   => wc_clean( $cart_item['pHeight'] ),
		'display' => '',
	);
	$item_data[] = array(
		'key'     => __( 'Bild', 'iconic' ),
		'value'   => wc_clean( $cart_item['selected_image'] ),
		'display' => '',
	);
	$item_data[] = array(
		'key'     => __( 'Bildausschnitt', 'iconic' ),
		'value'   => wc_clean( $cart_item['cropped_image'] ),
		'display' => '',
	);
		return $item_data;
	}
	
}
function iconic_add_engraving_text_to_order_items( $item, $cart_item_key, $values, $order ) {

	//if(empty($values['cropped_image'])) {
	//	return false;
	//}else{
		$item->add_meta_data( __( 'No of Platte', 'iconic' ), $values['no-of-pallets'] );
		$item->add_meta_data( __( 'Material', 'iconic' ), $values['selected_material'] );
		$item->add_meta_data( __( 'Platte 1 Breite', 'iconic' ), $values['width0'] );
		$item->add_meta_data( __( 'Platte 2 Breite', 'iconic' ), $values['width1'] );
		$item->add_meta_data( __( 'Platte 3 Breite', 'iconic' ), $values['width2'] );
		$item->add_meta_data( __( 'Höhe', 'iconic' ), $values['pHeight'] );
		$item->add_meta_data( __( 'Bildauschnitt', 'iconic' ), $values['selected_image'] );
		$item->add_meta_data( __( 'Cropped Image', 'iconic' ), $values['cropped_image'] );
	//}
}

	// //Add condition of Pallet tool product.
	// $tool_prod_id = get_option('current_post_id');

	// //Get current post id
	//global $post;
	// $product_id = $post->ID;

	// echo "***$tool_prod_id***";
	// echo $tool_prod_id;
	
	// echo "***$product_id***";
	// echo $product_id;

	//echo $post->post_content;
	$current_p_tool_id = get_option('current_p_tool_id');
	//if ( has_shortcode( $post->post_content, 'platetool' ) ) {
		//if ($tool_prod_id === $product_id ) {
	//global $product;
	//$product_id = $product->get_id();
	//$product_id = wc_get_product()->get_id();
	//$get_current_enabled = get_field('custom_tool',$current_p_tool_id);
	//echo $get_current_enabled;

	//if ( == 'Enable') {
		//echo "Exists....";
		add_filter( 'woocommerce_add_cart_item_data', 'iconic_add_engraving_text_to_cart_item', 10, 3 );
		add_filter( 'woocommerce_get_item_data', 'iconic_display_engraving_text_cart', 10, 2 );
		add_action( 'woocommerce_checkout_create_order_line_item', 'iconic_add_engraving_text_to_order_items', 10, 4 );
	//}

function ajax_enqueue() {
    wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/js/my-ajax-script.js', array('jquery') );
    wp_localize_script( 'ajax-script', 'my_ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}

add_action( 'wp_enqueue_scripts', 'ajax_enqueue' );
add_action( 'wp_ajax_foobar', 'wp_ajax_foobar_handler' );
add_action( 'wp_ajax_nopriv_foobar', 'wp_ajax_foobar_handler' );

//Save image to media
function save_image( $base64_img, $title ) {

	// Upload dir.
	$upload_dir  = wp_upload_dir();
	$upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

	$img             = str_replace( 'data:image/jpeg;base64,', '', $base64_img );
	$img             = str_replace( ' ', '+', $img );
	$decoded         = base64_decode( $img );
	$filename        = $title . '.jpeg';
	$file_type       = 'image/jpeg';
	$hashed_filename = md5( $filename . microtime() ) . '_' . $filename;

	// Save the image in the uploads directory.
	$upload_file = file_put_contents( $upload_path . $hashed_filename, $decoded );

	$attachment = array(
		'post_mime_type' => $file_type,
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $hashed_filename ) ),
		'post_content'   => '',
		'post_status'    => 'inherit',
		'guid'           => $upload_dir['url'] . '/' . basename( $hashed_filename )
	);

	$attach_id = wp_insert_attachment( $attachment, $upload_dir['path'] . '/' . $hashed_filename );
	return $attach_id;
}
// function wp_ajax_foobar_handler() {
// 	$clicked_image = $_REQUEST['clicked_image'];
// 	$x = $_REQUEST['x'];
// 	$y = $_REQUEST['y'];
// 	$w = $_REQUEST['w'];
// 	$h = $_REQUEST['h'];
// 	$targ_w = $w;
// 	$targ_h = $h;
//     $jpeg_quality = -1;

//     $src = $clicked_image;
//     $img_r = imagecreatefromjpeg($src);
//     $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

//     imagecopyresampled($dst_r,$img_r,0,0,$x,$y,$targ_w,$targ_h,$w,$h);
//     //header('Content-type: image/jpeg');
//     ob_start();
   
//     //imagejpeg($dst_r,'https://duschrueckwand-online.de/wp-content/plugins/plate-tool/simpletext.jpg',$jpeg_quality);
//     imagejpeg($dst_r,null,$jpeg_quality);
    
//     $cropped_image = ob_get_clean();
//     ob_end_clean();

//     // Free up memory
// 	//$cropped_image = imagedestroy($dst_r);

//     echo base64_encode( $cropped_image );
//     $title = "croppediamge";
//     //save_image( $cropped_image, $title );
//      //$cropped_image = imagedestroy( $dst_r );
//      //echo base64_encode( $cropped_image );
// 	 //$cropped_image = ob_get_clean();
//      //echo json_encode($cropped_image);
//      //echo "<img src='data:image/jpeg;base64,".base64_encode( $cropped_image )."'>";
//      //update_option( 'cropped_img', $cropped_img );    	
// }

function wp_ajax_foobar_handler() {
	    $clicked_image = $_REQUEST['lb_clicked_image'];
		$x = $_REQUEST['lb_x'];
		$y = $_REQUEST['lb_y'];
		$w = $_REQUEST['lb_cropperw'];
		//$h = $_REQUEST['lb_cropperh'];
		$h = $_REQUEST['lb_h'];
	    $jpeg_quality = 90;

	    $src = $clicked_image;
	    $img_r = imagecreatefromjpeg($src);
	    //print_r($w);
	    //echo "**********";
	    //print_r($h);
	    $dst_r = ImageCreateTrueColor( $w, $h );

	    imagecopyresampled($dst_r,$img_r,0,0,$x,$y,$w,$h,$w,$h);
	    //header('Content-type: image/jpeg');	
	    ob_start();
	   
	    //imagejpeg($dst_r,'https://duschrueckwand-online.de/wp-content/plugins/plate-tool/simpletext.jpg',$jpeg_quality);
	    
	    imagejpeg($dst_r,null,$jpeg_quality);
	    
	    $cropped_image = ob_get_clean();
	    //ob_end_clean();

	    // Free up memory
		//$cropped_image = imagedestroy($dst_r);

	    $base64 = base64_encode( $cropped_image );
	    //$attachment = chunk_split($base64);
	    //echo $attachment;
	    //echo "<img src='data:image/jpeg;base64,".$base64."'>";
	    
	    $title = "croppediamge";
	    $attachemntid = save_image( $base64, $title );
	    //echo $attachemntid;
	    $attachment_url = wp_get_attachment_url( $attachemntid );
	    //echo $attachment_url;
	    update_option( 'attachment_url', $attachment_url );
	    $attachment_url_option = get_option( 'attachment_url');
	    echo $attachment_url_option;
	    
	    //echo "<input id='cropped_url' type='text' value='".$attachment_url_option."'>";
	    //update_option( 'cropped_image_url', $attachment_url );
	    wp_die();
}